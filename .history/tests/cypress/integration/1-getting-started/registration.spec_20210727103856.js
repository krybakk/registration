describe("Registration", () => {

    it ('Shoud allow people to sign up', ()=> {
        cy.visit("http://localhost:8888");

        cy.get("#email").type("fake@email.com"); //znajdź pole email i wpisz do niego email

        const password = 'abcd1234'; //ustanowienie zmiennej hasła

        cy.get("#password").type(password); //znjadź pole hasło i wpisz hasło
        cy.get("#repeat-password").type(password); //znajdź pole "Powtórz hasło" i wpisz hasło

        cy.get("input[type='submit']").click(); //kliknij w przycisk

        cy.get("#welcome-screen").contains("Rejestracja zakończona!") //sprawdź czy po kliknieciu znajdziej napis "Rejestracja zakończona" i czy jest dostępny
        .should("be.visible");
    });
});