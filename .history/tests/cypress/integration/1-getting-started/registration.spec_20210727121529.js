describe("Registration", () => {
before (() => {
    cy.visit("http://localhost:8888")});

    it ('Should allow people to sign up/all data correct', ()=> {

        cy.get("#email").type("fake@email.com"); //znajdź pole email i wpisz do niego email

        const password = 'abcd1234'; //ustanowienie zmiennej hasła

        cy.get("#password").type(password); //znajadź pole hasło i wpisz hasło
        cy.get("#repeat-password").type(password); //znajdź pole "Powtórz hasło" i wpisz hasło

        cy.get("input[type='submit']").click(); //kliknij w przycisk

        cy.get("#welcome-screen").contains("Rejestracja zakończona!") //sprawdź czy po kliknieciu znajdziej napis "Rejestracja zakończona" i czy jest dostępny
        .should("be.visible");
    });

    it ('Should not allow people to sign up/no data', ()=> {

    });

});