describe("Registration", () => {
before (function () {
    cy.visit("http://localhost:8888")
    cy.fixture('testdata').then(function (testdata) {
        this.testdata = testdata
    })
})
    it ('Should allow people to sign up/all data correct', function () {

        cy.get('#txtEmail').type(this.testdata.email) //znajdź pole email i wpisz do niego email

        cy.get("#txtpassword").type(this.testdata.password) //znajadź pole hasło i wpisz hasło
        cy.get("#repeat-password").type(this.testdata.password) //znajdź pole "Powtórz hasło" i wpisz hasło

        cy.get("input[type='submit']").click(); //kliknij w przycisk

        cy.get("#welcome-screen").contains("Rejestracja zakończona!") //sprawdź czy po kliknieciu znajdziej napis "Rejestracja zakończona" i czy jest dostępny
        .should("be.visible")
    })

    // it ('Should not allow people to sign up/no data', ()=> {
    // });
})